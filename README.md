# CarCar

Team:

* Junjie - Sales
* Fermin Santiago - Service
## Design
Backend- Django & Python
Frontend - React & JavaScript
## Service microservice

Models - There are three models: AutomobileVO, Technician, and Appointment.
Integration - Inventory microservice will poll information and data from the Service microservice using the AutomobileVO. For each model you are able to make GET, POST, and DELETE requests methods.

## Sales microservice

Models - There are four models: AutomobileVO, Salesperson, Customer, and Sale.
Integration - Inventory microservice will poll information and data from the Sales microservice using the AutomobileVO. For each model you are able to make GET, POST, and DELETE requests methods.

