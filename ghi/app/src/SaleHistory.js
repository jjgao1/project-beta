import React, { useEffect, useState } from 'react';


function SaleHistory() {
	const [salespeople, setSalespeople] = useState([]);
	const [sales, setSales] = useState([]);
	const [salesperson, setSalesperson] = useState('');


	const handleSalespersonChange = (event) => {
		const value = parseInt(event.target.value);
		setSalesperson(value);
	}

	const loadSales = async () => {
		const url = "http://localhost:8090/api/sales/"
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setSales(data.sales)
		}
	}
	useEffect(() => {
		loadSales();
	}, [])

	const salespeopleData = async () => {
		const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
		const response = await fetch(salespeopleUrl);

		if (response.ok) {
			const data = await response.json();
			setSalespeople(data.salespeople)
		}
	}

	useEffect(() => {
		salespeopleData();
	}, []);

	const filteredSales = () => {
		const filtered = sales.filter(sale => sale.salesperson.id === salesperson);
		return filtered;
	}


	return (

		<><div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Salesperson History</h1>
					<form id="select-salesperson-form">
						<div className="mb-3">
							<select value={salesperson} onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
								<option>Choose a salesperson</option>
								{salespeople.map(person => {
									return (
										<option value={person.id} key={person.id}>
											{person.first_name + " " + person.last_name}
										</option>
									);
								})}
							</select>
						</div>
					</form>
				</div>
			</div>
		</div><table className="table table-striped">
				<thead>
					<tr>
						<th>Salesperson</th>
						<th>Customer</th>
						<th>VIN</th>
						<th>Price</th>
					</tr>
				</thead>
				<tbody>
					{filteredSales().map(sale => {
						return (
							<tr key={sale.id}>
								<td>{sale.salesperson.first_name + ' ' + sale.salesperson.last_name}</td>
								<td>{sale.customer.first_name + ' ' + sale.customer.last_name}</td>
								<td>{sale.automobile.vin}</td>
								<td>{'$' + sale.price}</td>
							</tr>
						);
					})}
				</tbody>
			</table></>
	);

};

export default SaleHistory
