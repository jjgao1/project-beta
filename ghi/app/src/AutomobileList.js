import React, { useEffect, useState } from "react";


function AutomobileList() {
	const [automobiles, setAutomobiles] = useState([]);

	const loadAutomobiles = async () => {
		const url = "http://localhost:8100/api/automobiles/";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setAutomobiles(data.autos);
		}
	};
	useEffect(() => {
		loadAutomobiles();
	}, []);

	const deleteAutomobile = async (vin) => {
		const url = `http://localhost:8100/api/automobiles/${vin}`;
		const response = await fetch(url, { method: "DELETE" });
		if (response.ok) {
			setAutomobiles(automobiles.filter((automobile) => automobile.id !== vin));
		}
	};

	return (
		<div>
			<h1>Automobiles</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Automobile VIN</th>
						<th>Color</th>
						<th>Year</th>
						<th>Model</th>
						<th>Manufacturer</th>
						<th>Sold</th>
						<th>Remove</th>
					</tr>
				</thead>
				<tbody>
					{automobiles?.map((automobile) => {
						return (
							<tr key={automobile.id}>
								<td>{automobile.vin}</td>
								<td>{automobile.color}</td>
								<td>{automobile.year}</td>
								<td>{automobile.model.name}</td>
								<td>{automobile.model.manufacturer.name}</td>
								<td>{automobile.sold ? "yes" : "no"}</td>
								<td>
									<button
										className="btn btn-secondary"
										onClick={() => deleteAutomobile(automobile.id)}
									>
										Remove
									</button>
								</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}

export default AutomobileList;
