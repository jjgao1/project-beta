import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import SalespersonList from "./SalespersonList";
import SalespersonForm from "./SalespersonForm";
import CustomerList from "./CustomerList";
import CustomerForm from "./CustomerForm";
import SaleList from "./SaleList";
import SaleForm from "./SaleForm";
import SaleHistory from "./SaleHistory";
import ManufacturerList from "./ManufacturerList";
import ManufacturerForm from "./ManufacturerForm";
import CarModelList from "./CarModelList";
import CarModelForm from "./CarModelForm";
import AutomobileList from "./AutomobileList";
import AutomobileForm from "./AutomobileForm";
import TechnicianList from "./TechnicianList";
import TechnicianForm from "./TechnicianForm";
import AppointmentList from "./AppointmentList";
import AppointmentForm from "./AppointmentForm";
import ServiceHistory from "./ServiceHistory";


function App() {
	return (
		<BrowserRouter>
			<Nav />
			<div className="container">
				<Routes>
					<Route path="/" element={<MainPage />} />
					<Route path="salespeople" element={<SalespersonList />} />
					<Route path="salespeople/new" element={<SalespersonForm />} />
					<Route path="customers" element={<CustomerList />} />
					<Route path="customers/new" element={<CustomerForm />} />
					<Route path="sales" element={<SaleList />} />
					<Route path="sales/new" element={<SaleForm />} />
					<Route path="sales/history" element={<SaleHistory />} />
					<Route path="manufacturers" element={<ManufacturerList />} />
					<Route path="manufacturers/new" element={<ManufacturerForm />} />
					<Route path="models" element={<CarModelList />} />
					<Route path="models/new" element={<CarModelForm />} />
					<Route path="automobiles" element={<AutomobileList />} />
					<Route path="automobiles/new" element={<AutomobileForm />} />
					<Route path="technicians" element={<TechnicianList />} />
					<Route path="technicians/new" element={<TechnicianForm />} />
					<Route path="appointments" element={<AppointmentList />} />
					<Route path="appointments/new" element={<AppointmentForm />} />
					<Route path="service-history" element={<ServiceHistory />} />
				</Routes>
			</div>
		</BrowserRouter>
	);
}

export default App;
