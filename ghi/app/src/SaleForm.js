import React, { useState, useEffect } from 'react';


function SaleForm() {
	const [automobiles, setAutomobiles] = useState([]);
	const [salespeople, setSalespeople] = useState([]);
	const [customers, setCustomers] = useState([]);
	const [automobile, setAutomobile] = useState('');
	const [salesperson, setSalesperson] = useState('');
	const [customer, setCustomer] = useState('');
	const [price, setPrice] = useState(0);


	const handleAutomobileChange = (event) => {
		const value = event.target.value;
		setAutomobile(value);
	}

	const handleSalespersonChange = (event) => {
		const value = event.target.value;
		setSalesperson(value);
	}

	const handleCustomerChange = (event) => {
		const value = event.target.value;
		setCustomer(value);
	}

	const handlePriceChange = (event) => {
		const value = event.target.value;
		setPrice(value);
	}

	const automobilesData = async () => {
		const automobilesUrl = 'http://localhost:8100/api/automobiles/';
		const response = await fetch(automobilesUrl);

		if (response.ok) {
			const data = await response.json();
			setAutomobiles(data.autos)
		}
	}

	useEffect(() => {
		automobilesData();
	}, []);

	const salespeopleData = async () => {
		const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
		const response = await fetch(salespeopleUrl);

		if (response.ok) {
			const data = await response.json();
			setSalespeople(data.salespeople)
		}
	}

	useEffect(() => {
		salespeopleData();
	}, []);

	const customersData = async () => {
		const customersUrl = 'http://localhost:8090/api/customers/';
		const response = await fetch(customersUrl);

		if (response.ok) {
			const data = await response.json();
			setCustomers(data.customers)
		}
	}

	useEffect(() => {
		customersData();
	}, []);


	const handleSubmit = async (event) => {
		event.preventDefault();

		const data = {};
		data.automobile = automobile;
		data.salesperson = salesperson;
		data.customer = customer;
		data.price = price


		const saleUrl = 'http://localhost:8090/api/sales/';
		const fetchConfig = {
			method: 'post',
			body: JSON.stringify(data),
			headers: {
				'Content-Type': 'application/json',
			},
		};

		const response = await fetch(saleUrl, fetchConfig);
		if (response.ok) {
			setAutomobile('');
			setSalesperson('');
			setCustomer('');
			setPrice(0);
		};
	};

	return (
		<div className="row">
			<div className="offset-3 col-6">
				<div className="shadow p-4 mt-4">
					<h1>Record a new sale</h1>
					<form onSubmit={handleSubmit} id="create-sale-form">
						<div className="mb-3">
							<select value={automobile} onChange={handleAutomobileChange} required id="automobile" name="automobile" className="form-select">
								<option>Choose an automobile VIN</option>
								{automobiles.map(auto => {
									return (
										<option value={auto.vin} key={auto.vin}>
											{auto.vin}
										</option>
									);
								})}
							</select>
						</div>
						<div className="mb-3">
							<select value={salesperson} onChange={handleSalespersonChange} required id="salesperson" name="salesperson" className="form-select">
								<option>Choose a salesperson</option>
								{salespeople.map(person => {
									return (
										<option value={person.id} key={person.id}>
											{person.first_name + " " + person.last_name}
										</option>
									);
								})}
							</select>
						</div>
						<div className="mb-3">
							<select value={customer} onChange={handleCustomerChange} required id="customer" name="customer" className="form-select">
								<option>Choose an customer</option>
								{customers.map(cust => {
									return (
										<option value={cust.id} key={cust.id}>
											{cust.first_name + " " + cust.last_name}
										</option>
									);
								})}
							</select>
						</div>

						<div className="form-floating mb-3">
							<input value={price} onChange={handlePriceChange} placeholder="0" required type="number" id="price" name="price" className="form-control" />
							<label htmlFor="price">Price</label>
						</div>
						<button className="btn btn-primary">Create</button>
					</form>
				</div>
			</div>
		</div>
	)
};

export default SaleForm
