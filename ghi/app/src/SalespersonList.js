import React, { useEffect, useState } from 'react';


function SalespersonList() {
	const [salespeople, setSalespeople] = useState([]);

	const loadSalespople = async () => {
		const url = "http://localhost:8090/api/salespeople/"
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setSalespeople(data.salespeople)
		}
	}
	useEffect(() => {
		loadSalespople();
	}, [])

	return (
		<div>
			<h1>Salepeople</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Employee ID</th>
						<th>First Name</th>
						<th>Last Name</th>
					</tr>
				</thead>
				<tbody>
					{salespeople.map(person => {
						return (
							<tr key={person.id}>
								<td>{person.employee_id}</td>
								<td>{person.first_name}</td>
								<td>{person.last_name}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);

};

export default SalespersonList
