import React, { useEffect, useState } from "react";


function CarModelList() {
	const [models, setModels] = useState([]);

	const loadModels = async () => {
		const url = "http://localhost:8100/api/models/";
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setModels(data.models);
		}
	};
	useEffect(() => {
		loadModels();
	}, []);

	return (
		<div>
			<h2>Models</h2>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>Name</th>
						<th>Manufacturer</th>
						<th>Picture</th>
					</tr>
				</thead>
				<tbody>
					{models.map((models) => {
						return (
							<tr key={models.id}>
								<td>{models.name}</td>
								<td>{models.manufacturer.name}</td>
								<td>{models.picture_url}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</div>
	);
}

export default CarModelList;
