import React, { useEffect, useState } from 'react';


function AppointmentList() {
	const [appointments, setAppointments] = useState([]);

	const loadAppointments = async () => {
		const url = "http://localhost:8080/api/appointments/"
		const response = await fetch(url);
		if (response.ok) {
			const data = await response.json();
			setAppointments(data.appointments)
		}
	}
	useEffect(() => {
		loadAppointments();
	}, [])

	const handleCancelAppointment = async (id) => {
		const cancelAppointmentUrl = `http://localhost:8080/api/appointments/${id}/cancel/`;
		const fetchConfig = {
			method: "PUT",
			body: JSON.stringify({ "status": "cancelled" }),
			headers: {
				"Content-Type": "application.json",
			},
		};
		const response = await fetch(cancelAppointmentUrl, fetchConfig)
		if (response.ok) {
			loadAppointments();
		}
	};


	const handleFinishAppointment = async (id) => {
		const finishAppointmentUrl = `http://localhost:8080/api/appointments/${id}/finish/`;
		const fetchConfig = {
			method: "PUT",
			body: JSON.stringify({ "status": "finished" }),
			headers: {
				"Content-Type": "application.json",
			},
		};
		const response = await fetch(finishAppointmentUrl, fetchConfig);
		if (response.ok) {
			loadAppointments();
		}
	};

	return (
		<div className="table-responsive">
			<h1>Service Appointments</h1>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>VIN</th>
						<th>Is VIP?</th>
						<th>Customer</th>
						<th>Date</th>
						<th>Time</th>
						<th>Technician</th>
						<th>Reason</th>
					</tr>
				</thead>
				<tbody>
					{appointments?.map(appointment => {
						if (appointment.status === "created") {
							return (
								<tr key={appointment.id}>
									<td>{appointment.vin}</td>
									<td>{appointment.vip}</td>
									<td>{appointment.customer}</td>
									<td>{new Date(appointment.date_time).toLocaleDateString()}</td>
									<td>{new Date(appointment.date_time).toLocaleTimeString()}</td>
									<td>{appointment.technician.first_name + ' ' + appointment.technician.last_name}</td>
									<td>{appointment.reason}</td>
									<td>
										<button className="btn btn-secondary" onClick={() => handleCancelAppointment(appointment.id)}>Cancel</button>
									</td>
									<td>
										<button className="btn btn-warning" onClick={() => handleFinishAppointment(appointment.id)}>Finish</button>
									</td>
								</tr>
							)
						};
					})}
				</tbody>
			</table>
		</div>
	);

};

export default AppointmentList
