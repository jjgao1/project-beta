from django.db import models
from django.urls import reverse


class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=50)

    def get_api_url(self):
        return reverse("api_technician_detail", kwargs={"pk": self.id})

    def __str__(self):
        return f"{self.first_name} {self.last_name}"


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    vin = models.CharField(max_length=20)


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=500)
    vin = models.CharField(max_length=17)
    vip = models.CharField(max_length=100, null=True)
    customer = models.CharField(max_length=200)
    status = models.CharField(max_length=20, default='created')
    technician = models.ForeignKey(
        Technician,
        related_name="technician",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("api_appointment", kwargs={"vin": self.vin})

    def __str__(self):
        return f"{self.date_time} - {self.customer} - {self.technician}"
